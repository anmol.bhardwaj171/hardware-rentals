import { NgModule } from '@angular/core';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { SharedModule } from './shared/shared.module';

import { AppComponent }   from './app.component';

import { AppRoutes } from './app.routing';
import { BrowserModule } from '@angular/platform-browser';
import { GraphQLModule } from './graphql.module';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from './auth/auth.service';
import { CallbackComponent } from './pages/callback/callback.component';
import { LoginComponent } from './features/login/login.component';
import { LogoutComponent } from './features/logout/logout.component';

@NgModule({
    imports:      [
        BrowserModule,
        
        RouterModule.forRoot(AppRoutes),
        HttpModule,
        SharedModule,
        GraphQLModule,
        HttpClientModule,
        
    ],
    declarations: [
        AppComponent,
        CallbackComponent,
        LoginComponent,
        LogoutComponent
    ],
    providers: [AuthService,],
    bootstrap:    [ AppComponent ]
})

export class AppModule { }
