import { Routes } from '@angular/router';
import { AdminLayoutComponent } from './shared/layouts/admin/admin-layout.component';
import { AuthGuard } from './core/auth/auth.guard';
// import { AuthLayoutComponent } from './shared/layouts/auth/auth-layout.component';

// import { AdminLayoutComponent } from '../../layouts/admin/admin-layout.component';
// import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
// import { Home1Component } from './features/Home/home1/home1.component'
import { CallbackComponent } from './pages/callback/callback.component';
import { LoginComponent } from './features/login/login.component';
import { LogoutComponent } from './features/logout/logout.component';

export const AppRoutes: Routes = [
    {
        path: 'login',
        canActivate: [AuthGuard],
        component: LoginComponent
    },
    {
        path: 'callback',
        component: CallbackComponent
    },
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
    },

      {
        path: 'dashboard',
        component: AdminLayoutComponent,
        canActivateChild: [AuthGuard],
        children: [{
            path: '',
            loadChildren: './shared/dashboard/dashboard.module#DashboardModule'
        }
    //     // ,{
    //     //     path: 'components',
    //     //     loadChildren: './components/components.module#ComponentsModule'
    //     // },{
    //     //     path: 'forms',
    //     //     loadChildren: './forms/forms.module#Forms'
    //     // },{
    //     //     path: 'tables',
    //     //     loadChildren: './tables/tables.module#TablesModule'
    //     // },{
    //     //     path: 'maps',
    //     //     loadChildren: './maps/maps.module#MapsModule'
    //     // },{
    //     //     path: 'charts',
    //     //     loadChildren: './charts/charts.module#ChartsModule'
    //     // },{
    //     //     path: 'calendar',
    //     //     loadChildren: './calendar/calendar.module#CalendarModule'
    //     // },{
    //     //     path: '',
    //     //     loadChildren: './userpage/user.module#UserModule'
    //     // },{
    //     //     path: '',
    //     //     loadChildren: './timeline/timeline.module#TimelineModule'
    //     // },{
    //     //     path: '',
    //     //     loadChildren: './widgets/widgets.module#WidgetsModule'
    //     // }
    ]
        },

    {
        path: 'logout',
        component: LogoutComponent
    },
        // {
        //     path: '',
        //     loadChildren: './shared/layouts/auth/auth-layout.module#AuthLayoutModule'
        // }
        // {
        //     path: 'dashboard',
        //     loadChildren: './features/Home/home.module#HomeModule'
        // }
        // {
        //     path: '',
        //     component: HomeComponent
        //   },
             
        
];
