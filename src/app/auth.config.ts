import { ENV } from './core/env.config';

interface AuthConfig {
  CLIENT_ID: string;
  CLIENT_DOMAIN: string;
//   AUDIENCE: string;
  REDIRECT: string;
//   SCOPE: string;
};

export const AUTH_CONFIG: AuthConfig = {
  CLIENT_ID: 'kzXOc6yor0aQy42CVD28k9MT2cNTJz5M',
  CLIENT_DOMAIN: 'hardware-rental.eu.auth0.com', // e.g., you.auth0.com
//   AUDIENCE: '[YOUR_AUTH0_API_AUDIENCE]', // e.g., http://localhost:8083/api/
  REDIRECT: `${ENV.BASE_URI}/callback`,
//   SCOPE: 'openid profile'
};