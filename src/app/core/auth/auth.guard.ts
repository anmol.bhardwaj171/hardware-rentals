import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'app/auth/auth.service';
// import { CookieService } from 'ngx-cookie';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  
  constructor(public auth: AuthService) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      console.log("11111111111111111111111111111111111111");
      
      if(!this.auth.loggingIn) {
        console.log("2222222222222222222222222222222222222222222222");
        
        if(!this.auth.loggedIn) {
          console.log("333333333333333333333333333333333333333333333");
          
            this.auth.login();
            return true;
            
        } else {
          this.auth.logout();
          return false;
        }
    } else {
      console.log("4444444444444444444444444444444");
      
      this.auth.logout();
      return false;
    }
  }


  canActivateChild(snapshot: ActivatedRouteSnapshot): boolean | Observable<boolean> | Promise<boolean> {
    
    if(!this.auth.loggingIn) {
      console.log("rrrrrrrrrrrrrrrrrrrrrrrrr");
      
      if(!this.auth.loggedIn) {
        console.log("xxxxxxxxxxxxxxxxxxxxxx");
        
          this.auth.login();
          return false;
          
      } else {

        return true;
      }
  } else {
    console.log("4444444444444444444444444444444");
    
    return true;
  }
  }
  
}
