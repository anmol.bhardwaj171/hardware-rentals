import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { LogoutComponent } from './logout.component';

@NgModule({
    declarations: [
        LogoutComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        // ErrorMessageModule
    ],
    providers: [],
    bootstrap: []
})
export class AuthLayoutModule { }
