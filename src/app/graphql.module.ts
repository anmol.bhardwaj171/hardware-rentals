import { HttpClientModule } from "@angular/common/http";
import { APOLLO_OPTIONS } from "apollo-angular";
import { HttpLinkModule, HttpLink } from "apollo-angular-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";
const uri = 'https://close-rat-93.hasura.app/v1/graphql'; // <-- add the URL of the GraphQL server here
import {NgModule} from '@angular/core';

@NgModule({
    providers: [
      {
        provide: APOLLO_OPTIONS,
        useFactory: (httpLink: HttpLink) => {
          return {
            cache: new InMemoryCache(),
            link: httpLink.create({
              uri: uri
            })
          }
        },
        deps: [HttpLink],
      },
    ],
    imports: [HttpLinkModule]
  })
export class GraphQLModule {}