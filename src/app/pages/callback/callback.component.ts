import { Component, OnInit } from '@angular/core';
import { AuthService } from 'app/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-callback',
  templateUrl: './callback.component.html',
  styleUrls: ['./callback.component.css']
})
export class CallbackComponent implements OnInit {

  constructor(private auth: AuthService, private router: Router) { 
  }
  
  ngOnInit(): void {
    this.auth.handleAuth();

    this.auth.loggedIn$.subscribe((val:any) => {
       this.router.navigate(['dashboard']);
      
    } )
    
  }

}
