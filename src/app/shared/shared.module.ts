import { NgModule } from '@angular/core';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


import { SidebarModule } from './sidebar/sidebar.module';
import { FixedPluginModule } from './shared/fixedplugin/fixedplugin.module';
import { FooterModule } from './shared/footer/footer.module';
import { NavbarModule} from './shared/navbar/navbar.module';
import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
// import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
    imports:      [
        CommonModule,
        FormsModule,
        NgbModule,
        SidebarModule,
        NavbarModule,
        FooterModule,
        FixedPluginModule,
        RouterModule
    ],
    declarations: [
        AdminLayoutComponent,
        // AuthLayoutComponent,
    ],
    exports: [CommonModule,
        FormsModule,
        NgbModule,
        SidebarModule,
        NavbarModule,
        FooterModule,
        FixedPluginModule,
        AdminLayoutComponent,
        // AuthLayoutComponent,
    ],

    bootstrap:    [  ]
})

export class SharedModule { }
